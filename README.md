# <div align="center">Telemedicina</div>
### <div align="center">CC6270-Sistemas Operacionais-Projeto</div>

## Time
|Integrante |RA |
| ------ | ------ |
| Elias Puttini da Cunha | 22118178-7 |
| Matheus Victor Alves da Silva | 22118184-5 |
| Lucas Costa Sampaio | 22118196-9 |



## Index
 **Entrega 1**
- [Introdução](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#introdu%C3%A7%C3%A3o)
- [Hardware Utilizado](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#hardware-utilizado)
- [Software Utilizado](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#software-utilizado)
- [Conceituação do sistema operacional](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#conceitua%C3%A7%C3%A3o-do-sistema-operacional) 
- [Em relação ao Hardware](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#em-rela%C3%A7%C3%A3o-ao-hardware)

 **Entrega 2**
- [Processos](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#processos)
- [Threads](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#threads)
- [Escalonamento de processos](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#escalonamento-de-processos)
 
 **Entrega 3**
- [Script](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#script)
- [comandos PowerShell](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#comandos-unixpowershell)
 
 **Entrega 4**
- [Gerenciamento de memória](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#gerenciamento-de-mem%C3%B3ria)
 
 **Entrega 5**
- [Sistema de arquivos PowerShell](https://gitlab.com/fei-6-ciclo/sistemas-operacionais/-/blob/master/README.md#sistema-de-arquivos-powershell)


## Hardware Utilizado
- CPU intel i5 7º Geração
- GPU Nvidea GTX 1050
- RAM 16GB
- Sistema Operacional Windows

## Software Utilizado
- Windows PowerShell ISE


## Introdução
- A telemedicina é uma especialidade médica que disponibiliza serviços a distância para o cuidado com a saúde, o que ocorre por meio de modernas tecnologias digitais que promovem a assistência médica online a pacientes, clínicas, hospitais e profissionais da saúde. Este intercâmbio de informações acontece através da internet, em plataformas online para acesso pelo computador, celular ou tablet, que garantem alta velocidade no acolhimento.

## Conceituação do sistema operacional
- Sera desenvolvido um sistema de agendamento de consultas medicas para o facilitar o atendimento de clientes em clinicas pequenas. O sistema devera receber um input contendo o cadastro do cliente e aloca-lo em um dos horarios disponiveis para consulta com determinado medico, o horario selecionado deve sair da lista de horarios disponiveis.

## Em relação ao Hardware
- O sistema deve ser capaz de rodar em qualquer aparelho que possua os requisitos minimos para suporte ao sitemas operacional windows 10
- Minimo:
- Processador de 1 gigahertz (GHz) 
- 1 gigabyte (GB) de memoria RAM
- 16 gigabyte (GB) de memoria interna
- Placa grafica com DirectX 9


## Processos
- Os chamados “processos” são módulos executáveis, os quais contêm linhas de código para que a execução do programa seja realizada apropriadamente. Isso quer dizer que o processo é uma lista de instruções, a qual informa ao processador que passos devem ser executados e em quais momentos isso acontece.

- Os processos podem estar nos seguintes estados:

        **NEW** - estado de criação

        **READY** - aguarda direcionamento a uma unidade de processamento

         **RUNNING** - executa comandos

         **WAITING** - aguarda um evento

         **TERMINATED** - execução finalizada


- Os processos tambem podem classificados entre **CPU-bound** ou **I/O-bound**:

         O CPU-bound passa a maior parte do tempo em estado de execução usando a CPU

         O I/O-bound executa um grande numero de operções de input e output

- no projeto toda função sera um processo

## Threads
- Linha ou Encadeamento de execução, é uma forma de um processo dividir a si mesmo em duas ou mais tarefas que podem ser executadas concorrentemente.

- Threads são utilizadas no projeto para trabalhemos simultaneamente com a interface do usuario enquanto aguarda as atualizações na agenda feita por outros usuários

## Escalonamento de processos
- O escalonamento de processos ou agendador de tarefas é uma atividade organizacional feita pelo escalonador da CPU ou de um sistema distribuído, possibilitando executar os processos mais viáveis e concorrentes, priorizando determinados tipos de processos.

- não foi utilizado escalonamento de processos


## Script
- O script é a sequência de passos que o computador vai interpretar para somar e apresentar as notas na tela. Isso também pode ser feito para modificar uma fonte que será exibida em algum site ou programa, repetições etc.

- Nos scripts do projeto utilizamos de loops, manipulação de arquivos e criação de pastas.

## comandos PowerShell
- O nosso grupo decidiu usar PowerShell por causa da Facilidade de acesso. 
Em computação, um shell (em português, casca ou concha) é uma interface de usuário para acessar os serviços de um sistema operacional. Em geral, shells dos sistemas operacionais usam uma interface de linha de comando (ILC) ou uma interface gráfica de usuário (IGU), dependendo da função e operação particular de um computador. macOS e Windows são exemplos de sistemas operacionais amplamente utilizados através de interfaces gráficas.



## --
![Image 1](./img/codigoV1.png)
![Image 2](./img/codigoV1_cadastro.png)

## Gerenciamento de memória 

O PowerShell não fornece nenhuma maneira integrada de controlar os recursos do sistema, como a memória usada por um script. Impossibilitando de controlar quais regiões da memória são utilizadas e por quais processos. 
Decidir qual processo deve ser carregado para a memória quando houver espaço disponível e alocar e desalocar espaço de memória sem a ajuda de algum software é barrado pelo Windows.
O PowerShell do windows é limitado devido as politicas de segurança do sistema, nos privando a apenas mostrarmos a quantidade de memória disponível e suas informações com o comando a seguir:
```
Get-WmiObject win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize
```
Porém ainda é possivel guardar variaveis para programar, porém elas são alocadas no sistema de forma aleatória:

```
$MyVariable = 1, 2, 3

```


## Perguntas
Seguem perguntas que poderão auxiliar na conceituação e exemplificação do gerenciamento de memória

**1) O software desenvolvido funcionará local ou remotamente?**

Localmente, como o objetivo é que as consultas sejam marcadas dentro da clinica não é necessario fazer o acesso remoto. 

**2) A leitura das variáveis referentes ao ambiente operacional será feita via input do usuário ou o próprio software fará esta entrada dos dados?**

A leitura dos dados sera feita pelo o software, o usuario vai fazer a inserção pela interface gráfica do software.

**3) Há atualizações (hardware e software) necessárias que devem ser feitas e monitoradas para que o projeto desenvolvido funcione?**

O nosso software funciona em todas as versoes do windows (Windows XP, Windows Vista/7 e Windows 10), porem como o suporte do Windows XP e Windows Vista/7 acabou nos recomendamos o windows 10 para rodar o software

Não é necessario fazer o monitoramento do software, mas no caso do hardware é bom verificar o espaço que o software está usando para salvar os aruqivos para que o mesmo não lote (é possivel fazer isso com a limpesa de consultas antigas que estao no sistema).

**4) Há licenças de software que devem ser consideradas?**

Não, o software não possui nenhuma licença.

## Sistema de arquivos PowerShell

Para o sistem de gerenciamento de arquivos o NTFS (New Technology File System) é o sistema padrao utilizado pelos sistemas baseados em windows e possui uma estrutura que armazena as localizações de todos os arquivos e diretórios, incluindo os arquivos referentes ao próprio sistema de arquivos denominado MFT (Master File Table)



## Segurança
O PowerShell do windows necessita de permissão de administrador para que possa rodar scripts que utilizam funções relacionadas a processos da máquina e seus respectivos componentes.

Para habilitar a execução de scritps (execute o powershell como administrador):
```
Set-ExecutionPolicy Unrestricted
```
De qualquer forma, o sistema restringe bastante o usuário, impossibilitando por exemplo, que ele administre a memória ou outro componente. Se limitando a funções básicas da máquina como gerar uma interface gráfica e mostrar informações, calculos etc

Em resumo o PowerShell possibilita o usuário a mostrar todas as informações da máquina, mas limita a alteração delas.

## Perguntas

Sobre montar um sistema de arquivo no linux

**1) Como listar os sistemas de arquivos montados?**

```
fdisk -l
```
obs: para usar o comando você precsisa estar dentro da pasta que foi criada a partição

**2) Quais são os passos para montar um sistema de arquivos no linux? Faça uso de um ou mais sistemas de arquivos à sua escolha. Sugere-se que o sistema de arquivo escolhido suporte o projeto que desenvolve.**

1. Criar uma pasta onde será montada a partição Windows NTFS.

Nesse caso criaremos uma pasta chamada "Windows10" dentro da pasta "media" no diretório raiz (/media).

```
mkdir /media/Windows10
```

2. Montar a partição na pasta criada, no diretório /media/Windows10.
```
mount -t ntfs-3g -o remove_hiberfile /dev/sda2/media/Windows10
```
Pronto, basta abrir, clicando no dispositivo ou abrindo a pasta Windows10 em /media/, os arquivos estarão nela.

**3) Como desmontar um sistema de arquivo?**

Se for necessário desmontar a partição, basta utilizar o comando umount no mesmo caminho do diretório onde foi montada anteriormente.
```
umount /media/Windows10
```    
